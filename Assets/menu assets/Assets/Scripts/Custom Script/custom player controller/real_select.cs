﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SelectCharacterfull : MonoBehaviour {

	public void select (int index, int player)
	{
		Debug.Log(index);
		if(index == SelectCharacter.selectionIndex || index < 0 || index >= SelectCharacter.models.Count)
			return;

		if(player == 1)
		{
			Debug.Log("mouse button or joystick 1 used");
			SelectCharacter.models[SelectCharacter.selectionIndex].SetActive(false);
			SelectCharacter.selected_p1 = index;
			SelectCharacter.selectionIndex = index;
			SelectCharacter.models[SelectCharacter.selectionIndex].SetActive(true);
		}

		if(player == 2)
		{
			Debug.Log("joystick 2 used");
			SelectCharacter.models[SelectCharacter.selectionIndex + (SelectCharacter.models.Count / 2)].SetActive(false);
			SelectCharacter.selected_p2 = index;
			SelectCharacter.selectionIndex = index;
			SelectCharacter.models[SelectCharacter.selectionIndex + (SelectCharacter.models.Count / 2)].SetActive(true);
		}

	}
}