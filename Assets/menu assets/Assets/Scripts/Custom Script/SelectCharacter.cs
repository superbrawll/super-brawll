﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SelectCharacter : MonoBehaviour {

	static public List<GameObject> models;
	//Default index model
	static public int selectionIndex = 0;
	static public int selected_p1 = 0;
	static public int selected_p2 = 0;

	private void Start()
	{
		models = new List<GameObject>();
		foreach(Transform t in transform)
		{
			foreach(Transform i in t)
			{
				models.Add(i.gameObject);
				i.gameObject.SetActive(false);
			}
		}

		models[selectionIndex].SetActive(true);
		models[models.Count / 2].SetActive(true);
	}
}
