﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {

	/*public void LoadByIndex (int sceneindex)
	{
		SceneManager.LoadScene (sceneindex);
	}*/
	public string scene;
	public Color loadToColor = Color.white;
	
	public void GoFade()
    {
        Initiate.Fade(scene, loadToColor, 1.0f);
    }
}
