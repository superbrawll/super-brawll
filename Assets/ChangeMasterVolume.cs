﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeMasterVolume : MonoBehaviour {

    public Slider Volume;
    public AudioSource myMusic;
    public GameObject music;
    void Start()
    {
        DontDestroyOnLoad(music);
    }

    // Update is called once per frame
    void Update()
    {
        myMusic.volume = Volume.value;
    }
}
