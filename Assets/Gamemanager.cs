﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gamemanager : MonoBehaviour
{
    public static Gamemanager instance;
    public Text score;
    int P1score;
    int P2score;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void Playerkilled(PlayerMain player)
    {
        switch (player.playerid)
        {
            case Player.P1:
                P2score++;
                    break;
            case Player.P2:
                P1score++;
                break;
        }
        score.text = "<color=red>"+P1score+"</color> - <color=blue>"+P2score+"</color>";
        StartCoroutine(respawn(player));
    }

    IEnumerator respawn(PlayerMain player)
    {
        yield return new WaitForSeconds(2f);
        player.transform.position = Vector2.zero;
        player.Ballcollision.SetActive(false);
        Color stored = player.sprite.color;
        Color n = stored;
        n.a = 0.35f;
        player.sprite.color = n;
        player.gameObject.SetActive(true);
        yield return new WaitForSeconds(3f);
        player.Ballcollision.SetActive(true);
        player.sprite.color = stored;
    }
}
