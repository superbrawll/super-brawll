﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneSelect : MonoBehaviour {

	public string button = "";

	public void FixedUpdate()
	{
		if(Input.GetKeyDown("joystick 1 button 0"))
		{
			Debug.Log("player 1 pressed");
			button = "player_1";
		}
		if(Input.GetKeyDown("joystick 2 button 0"))
		{
			Debug.Log("player 2 pressed");
			button = "player_2";
		}
	}

	//index is given in the onclick() unity manager
	public void select (int index)
	{
		Debug.Log(index);
		//we check that index is in the range of the models list and is not equal to selected_p1/p2
		if(index == SelectCharacter.selected_p1 || index == SelectCharacter.selected_p2 ||
		   index < 0 || index >= SelectCharacter.models.Count)
			return;

		//we check if the controller used is P1 and update the visual and variables
		if(button == "player_1")
		{
			Debug.Log("mouse button or joystick 1 used");
			SelectCharacter.models[SelectCharacter.selectionIndex].SetActive(false);
			SelectCharacter.selected_p1 = index;
			SelectCharacter.selectionIndex = index;
			SelectCharacter.models[SelectCharacter.selectionIndex].SetActive(true);
		}

		//we check if the controller used is P2 and update the visual and variables
		if(button == "player_2")
		{
			Debug.Log("joystick 2 used");
			SelectCharacter.models[SelectCharacter.selectionIndex + (SelectCharacter.models.Count / 2)].SetActive(false);
			SelectCharacter.selected_p2 = index;
			SelectCharacter.selectionIndex = index;
			SelectCharacter.models[SelectCharacter.selectionIndex + (SelectCharacter.models.Count / 2)].SetActive(true);
		}

	}
}
