﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallControl : MonoBehaviour {
    public PlayerMain Main;

    public float Charge;
    public float maxForce;
    bool Grabaxisused;
    bool Throwaxisused;
    bool grabbing;
    bool charging;
    public float Grabtiming;
    public float GrabDelay;


    bool cangrab = true;
    Vector2 AimVector;
    public float Smashdelay;
    bool LemmeSmash = false;


    public BallStorage ballstorage;
    [System.Serializable]
    public class BallStorage
    {
        public int Ballnum = 0;
        public List<Transform> Storepoints = new List<Transform>();
        public List<Transform> StoredBalls = new List<Transform>();
    }

    public Visuals visuals;
    [System.Serializable]
    public class Visuals
    {
        public Slider slider;
        public GameObject sheild;
        public GameObject Shieldui;
        public ParticleSystem Chargeanim;
        public ParticleSystem Throwparticle;
        public ParticleSystem Smashparticle;
    }

    Coroutine _Grab;
    Coroutine _GrabCooldown;

    private void Start()
    {
        Main = GetComponent<PlayerMain>();
    }

    private void Update()
    {
        AimVector.x = Input.GetAxis("FHorizontal_" + Main.playerid);
        AimVector.y = Input.GetAxis("FVertical_" + Main.playerid);
        AimVector.Normalize();

        if (Input.GetAxis("Grab_" + Main.playerid) > 0f && !Grabaxisused)
            {
                Grabaxisused = true;
                if (cangrab && !charging)
                {
                    if (!grabbing)
                    {
                    Debug.Log("GRAB");
                        _Grab = StartCoroutine(Grab());
                    }
                    else
                    {
                    Debug.Log("Interruptgrab");
                        StopGrab();
                        _GrabCooldown = StartCoroutine(Grabdelay());
                    }
                }
            }
        //Debug.Log(Input.GetAxis("Grab_" + Main.playerid));
        if (Input.GetAxis("Grab_" + Main.playerid) < 1f)
            {
                Grabaxisused = false;
            }
        if (Input.GetAxis("Throw_" + Main.playerid) > 0f && !Throwaxisused && ballstorage.Ballnum > 0)
        {
            if(charging == false)
            {
                visuals.Chargeanim.Play();
            }
                charging = true;
                Main.PlayerControl.Speedmod = 0.4f;
                if (Charge < 1)
                {
                    Charge += 0.01f;
                    visuals.slider.value = Charge;
                }
                else
                {
                    Charge = 1;
                visuals.slider.value = Charge;
                    Throw(Charge);
                }
        }
        if (Input.GetAxis("Throw_" + Main.playerid) < 1f)
        {
            Throwaxisused = false;
            if (charging)
            {
                Throw(Charge);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Ball" && !Main.Playerdash.invulnerable)
        {
            Ball ball = collision.GetComponentInParent<Ball>();
            if (ball != null)
            {
                if (ballstorage.Ballnum < 3 && (ball.Owner == Player.N || grabbing))
                {
                    if (grabbing)
                    {
                        StopGrab();
                    }
                    if (grabbing || ball.power > 0)
                    {
                        LemmeSmash = true;
                        visuals.Smashparticle.Play();
                        StartCoroutine(Smashcooldown());
                    }
                    Pickup(ball);
                }
                else if (ball.Owner != Main.playerid)
                {
                    Pickup(ball);
                    Hit();
                }
            }
        }
        if(collision.tag == "Player" && Main.Playerdash.dashing && Main.Playerdash.cansteal)
        {
            Main.Playerdash.cansteal = false;
            BallControl player = collision.transform.parent.GetComponent<BallControl>();
            if(player != null && player.Main.playerid != Main.playerid && ballstorage.Ballnum <3 && player.ballstorage.Ballnum > 0)
            {
                player.Steal(this);
            }
        }
    }

    public void Steal(BallControl Theif)
    {
        if (charging)
        {
            Interrupt();
        }
        Theif.Pickup(ballstorage.StoredBalls[ballstorage.Ballnum - 1].GetComponent<Ball>());
        ballstorage.StoredBalls.RemoveAt(ballstorage.Ballnum - 1);
        ballstorage.Ballnum--;

    }

    public void Interrupt()
    {
        charging = false;
        Throwaxisused = true;
        visuals.Chargeanim.Stop();
        visuals.Chargeanim.Clear();
        Main.PlayerControl.Speedmod = 1f;
        Charge = 0;
        visuals.slider.value = Charge;
    }

    public void Pickup(Ball ball)
    {
        ballstorage.Ballnum++;
        grabbing = false;
        ball.Ghost();
        ball.Owner = Main.playerid;
        ball.GetComponentInChildren<Renderer>().material = Main.Ballmat;
        ball.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        ball.transform.position = ballstorage.Storepoints[ballstorage.Ballnum -1].transform.position;
        ball.transform.parent = ballstorage.Storepoints[ballstorage.Ballnum -1].transform;
        ballstorage.StoredBalls.Add(ball.transform);
    }

    public void Hit()
    {
        gameObject.SetActive(false);
        Gamemanager.instance.Playerkilled(Main);
    }

    public void Throw(float Power)
    {
        charging = false;
        Throwaxisused = true;
        visuals.Chargeanim.Stop();
        visuals.Chargeanim.Clear();
        visuals.Throwparticle.Play();
        Main.PlayerControl.Speedmod = 1f;
        Ball ball = ballstorage.StoredBalls[ballstorage.Ballnum - 1].GetComponent<Ball>();
        if (LemmeSmash)
        {
            LemmeSmash = false;
            visuals.Smashparticle.Stop();
            ball.Throw(ball.power, AimVector);
        }
        else
        {
            ball.Throw(Power * maxForce, AimVector);
        }
        ball.transform.parent = null;
        if (AimVector.magnitude == 0)
        {
            Debug.Log("Sike!");
            ball.SetMine();
        }
        ballstorage.StoredBalls.RemoveAt(ballstorage.Ballnum - 1);
        ballstorage.Ballnum--;
        Charge = 0;
        visuals.slider.value = Charge;
    }

    void StopGrab()
    {
        StopCoroutine(_Grab);
        if (_GrabCooldown != null)
        {
            StopCoroutine(_GrabCooldown);
        }
        grabbing = false;
        cangrab = true;
        visuals.Shieldui.SetActive(true);
        visuals.sheild.SetActive(false);
    }

    IEnumerator Grab()
    {
        grabbing = true;
        cangrab = false;
        visuals.Shieldui.SetActive(false);
        visuals.sheild.SetActive(true);
        yield return new WaitForSeconds(Grabtiming);
        StopGrab();
        _GrabCooldown = StartCoroutine(Grabdelay());
    }
    IEnumerator Grabdelay()
    {
        cangrab = false;
        visuals.Shieldui.SetActive(false);
        yield return new WaitForSeconds(GrabDelay);
        cangrab = true;
        visuals.Shieldui.SetActive(true);
    }

    IEnumerator Smashcooldown()
    {
        yield return new WaitForSeconds(Smashdelay);
        LemmeSmash = false;
        visuals.Smashparticle.Stop();
    }

    private void OnDrawGizmos()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, AimVector, Mathf.Infinity, 1 << 8);
        if (hit)
        {
            Debug.DrawRay(transform.position, AimVector * 2, Color.red);
            Gizmos.DrawSphere(hit.point + (hit.normal / 2), 0.5f);
            Debug.DrawRay(hit.point + (hit.normal / 2), Vector2.Reflect(AimVector, hit.normal) * 2, Color.red);
        }
    }
}
