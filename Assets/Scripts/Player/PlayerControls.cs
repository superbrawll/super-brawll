﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour {
    public PlayerMain Main;

    public float Speed;
    public Rigidbody2D rb;
    public GameObject PlayerModel;
    public Vector2 MoveVector;
    public float Speedmod;

	void Start () {
        Main = GetComponent<PlayerMain>();
        rb = GetComponent<Rigidbody2D>();
	}
	
	void Update () {
        MoveVector.x = Input.GetAxis("Horizontal_" + Main.playerid);
        MoveVector.y = Input.GetAxis("Vertical_" + Main.playerid);
        MoveVector.Normalize();
    }

    private void FixedUpdate()
    {
        if (!Main.Playerdash.dashing)
        {
            rb.velocity = new Vector3(MoveVector.x * Speed*Speedmod, MoveVector.y * Speed* Speedmod);
        }
    }
}

