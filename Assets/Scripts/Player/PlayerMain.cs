﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Player { N, P1, P2, P3, P4 };

public class PlayerMain : MonoBehaviour {
    public Player playerid;
    public Material Ballmat;

    public GameObject Ballcollision;
    public SpriteRenderer sprite;

    public BallControl Ballcontrol;
    public PlayerControls PlayerControl;
    public PlayerDash Playerdash;

    private void Start()
    {
        Ballcontrol = GetComponent<BallControl>();
        PlayerControl = GetComponent<PlayerControls>();
        Playerdash = GetComponent<PlayerDash>();
    }
}
