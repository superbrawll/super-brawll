﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDash : MonoBehaviour {
    public PlayerMain Main;

    public float DashForce;
    public float DashTime;
    public float protecttime;
    public bool dashing;
    public ParticleSystem Dasheffect;
    public bool invulnerable;
    public bool cansteal;
    bool candash = true;

    private void Start()
    {
        Main = GetComponent<PlayerMain>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Dash_" + Main.playerid) && candash)
        {
            Main.PlayerControl.rb.velocity = DashForce * Main.PlayerControl.MoveVector;
            Dasheffect.Play();
            StartCoroutine(Dash());
        }
    }

    IEnumerator Dash()
    {
        candash = false;
        cansteal = true;
        dashing = true;
        invulnerable = true;
        yield return new WaitForSeconds(DashTime);
        dashing = false;
        yield return new WaitForSeconds(protecttime);
        invulnerable = false;
        cansteal = false;
        StartCoroutine(DashDelay());
    }

    IEnumerator DashDelay()
    {
        yield return new WaitForSeconds(0.5f);
        candash = true;
    }
}
