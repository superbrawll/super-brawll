﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    public Player Owner;
    public float power;
    public Vector2 direction;
    public Rigidbody2D rb;
    public GameObject PlayerTrigger;
    public GameObject BallCollision;
    public ParticleSystem hitparticles;
    public Material ballmat;
    public ParticleSystem Chargeparticle;

    void Start () {
        rb = GetComponent<Rigidbody2D>();
	}

    private void FixedUpdate()
    {
        rb.velocity = direction * power;
    }

    public void Ghost()
    {
        Chargeparticle.Stop();
        Chargeparticle.Clear();
        rb.isKinematic = true;
        direction = Vector2.zero;
        PlayerTrigger.SetActive(false);
        BallCollision.SetActive(false);
    }

    public void SetMine()
    {
        PlayerTrigger.SetActive(false);
        Owner = Player.N;
        GetComponentInChildren<Renderer>().material = ballmat;
        ParticleSystem.EmissionModule emission = Chargeparticle.emission;
        emission.rateOverTime = Mathf.Lerp(5,50,power/100);
        Chargeparticle.Play();
        StartCoroutine(MineCooldown());
    }

    public void Reset()
    {
        Owner = Player.N;
        GetComponentInChildren<Renderer>().material = ballmat;
        power = 0;
        Chargeparticle.Stop();
    }

    public void Throw(float _Power,Vector2 _Direction)
    {
        rb.isKinematic = false;
        PlayerTrigger.SetActive(true);
        BallCollision.SetActive(true);
        power = _Power;
        direction = _Direction;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        StartCoroutine(Bounce(collision.GetContact(0)));
    }

    IEnumerator MineCooldown()
    {
        yield return new WaitForSeconds(2f);
        PlayerTrigger.SetActive(true);
    }

    IEnumerator Bounce(ContactPoint2D hit)
    {
        hitparticles.transform.position = hit.point;
        hitparticles.Play();
        Vector2 rf = Vector2.Reflect(direction.normalized, hit.normal);
        rb.velocity = Vector3.zero;
        direction = rf;
        yield return null;
    }
}
