﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler {
	//Background Image
	private Image BgImg;
	//Joystick Image
	private Image joyImg;
	private Vector3 InputVector;

	void Start(){
		BgImg = GetComponent<Image> ();
		joyImg = transform.GetChild (0).GetComponent<Image> ();
	}

	public virtual void OnDrag(PointerEventData pad){
		Vector2 pos;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (BgImg.rectTransform, pad.position, pad.pressEventCamera, out pos)) {
			pos.x = (pos.x / BgImg.rectTransform.sizeDelta.x);
			pos.y = (pos.y / BgImg.rectTransform.sizeDelta.y);

			InputVector = new Vector3 (pos.x * 2,0, pos.y * 2);
			InputVector = (InputVector.magnitude> 1.0f) ? InputVector.normalized : InputVector;

			joyImg.rectTransform.anchoredPosition = new Vector3 (InputVector.x*(BgImg.rectTransform.sizeDelta.x/3),InputVector.z*(BgImg.rectTransform.sizeDelta.y/3));
		}
	}

	public virtual void OnPointerDown(PointerEventData pad){
		OnDrag (pad);
	}

	public virtual void OnPointerUp(PointerEventData pad){
		InputVector = Vector3.zero;
		joyImg.rectTransform.anchoredPosition = Vector3.zero;
	}

	public float Horizontal(){
			return InputVector.x;
	}
	public float Vertical(){
			return InputVector.z;
	}
}
